const gameBoard = document.getElementById('game-board');
const scoreElement = document.getElementById('score');
let snake = [{ top: 10, left: 10 }];
let direction = { top: 0, left: 1 };
let food = null;
let score = 0;

function updateGame() {
  // Update snake position
  const head = { ...snake[0] }; // copy head
  head.top += direction.top;
  head.left += direction.left;
  snake.unshift(head);

  if (food && food.top === head.top && food.left === head.left) {
    // Eat food
    score++;
    scoreElement.textContent = score;
    food = null;
  } else {
    // Remove tail
    snake.pop();
  }

  // Game over
  if (
    head.top < 0 ||
    head.left < 0 ||
    head.top > gameBoard.clientHeight / 10 ||
    head.left > gameBoard.clientWidth / 10 ||
    snake.find((part, index) => index !== 0 && part.top === head.top && part.left === head.left)
  ) {
    snake = [{ top: 10, left: 10 }];
    direction = { top: 0, left: 1 };
    food = null;
    score = 0;
    scoreElement.textContent = score;
  }

  // Generate new food
  while (!food) {
    const left = Math.floor(Math.random() * gameBoard.clientWidth / 10);
    const top = Math.floor(Math.random() * gameBoard.clientHeight / 10);
    if (!snake.find(part => part.top === top && part.left === left)) {
      food = { top, left };
    }
  }

  // Clear game board
  while (gameBoard.firstChild) {
    gameBoard.firstChild.remove();
  }

  // Draw snake
  snake.forEach(part => {
    const snakeElement = document.createElement('div');
    snakeElement.style.top = `${part.top * 10}px`;
    snakeElement.style.left = `${part.left * 10}px`;
    snakeElement.classList.add('snake');
    gameBoard.appendChild(snakeElement);
  });

  // Draw food
  const foodElement = document.createElement('div');
  foodElement.style.top = `${food.top * 10}px`;
  foodElement.style.left = `${food.left * 10}px`;
  foodElement.classList.add('food');
  gameBoard.appendChild(foodElement);
}

setInterval(updateGame, 100);

window.addEventListener('keydown', e => {
  switch (e.key) {
    case 'ArrowUp':
      if (direction.top !== 1) direction = { top: -1, left: 0 };
      break;
    case 'ArrowDown':
      if (direction.top !== -1) direction = { top: 1, left: 0 };
      break;
    case 'ArrowLeft':
      if (direction.left !== 1) direction = { top: 0, left: -1 };
      break;
    case 'ArrowRight':
      if (direction.left !== -1) direction = { top: 0, left: 1 };
      break;
  }
});
